package com.eauction.seller.deleteproduct.service;

import com.eauction.seller.deleteproduct.dto.ProductDeleteInputDTO;
import com.eauction.seller.deleteproduct.entity.ProductDetail;
import com.eauction.seller.deleteproduct.exception.BidEndedException;
import com.eauction.seller.deleteproduct.exception.BidExistsException;
import com.eauction.seller.deleteproduct.repository.BidDetailsRepository;
import com.eauction.seller.deleteproduct.repository.ProductDetailsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.PublishResponse;
import software.amazon.awssdk.utils.CollectionUtils;
import software.amazon.awssdk.utils.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Log4j2
public class DeleteProductService {

    private final ProductDetailsRepository productDetailsRepository;
    private final BidDetailsRepository bidDetailsRepository;
    private final SnsClient snsClient;

    public ProductDetail deleteProductDetails(ProductDeleteInputDTO productDeleteInputDTO) {
        log.debug("Received input: {}", productDeleteInputDTO);
        if(Objects.nonNull(productDeleteInputDTO) && !StringUtils.isEmpty(productDeleteInputDTO.getProductId())) {
            validateBidAssociation(productDeleteInputDTO.getProductId());
            validateBidEndDateOfProduct(productDeleteInputDTO.getProductId());
            sendProductDeleteNotification(productDeleteInputDTO);
            return productDetailsRepository.deleteProduct(productDeleteInputDTO.getProductId());
        }
        return new ProductDetail();
    }

    private void validateBidAssociation(String productId) {
        if(!CollectionUtils.isNullOrEmpty(bidDetailsRepository.fetchBids(productId))) {
            throw new BidExistsException("Product cannot be deleted. One or more Bids associated with this product.");
        }
        log.debug("validateBidAssociation passed");
    }

    private void validateBidEndDateOfProduct(String productId) {
        ProductDetail productDetail = productDetailsRepository.fetchProductById(productId);
        if(Objects.nonNull(productDetail) && !StringUtils.isEmpty(productDetail.getBidEndDate())) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate bidDate = LocalDate.parse(productDetail.getBidEndDate(),formatter);
            if(bidDate.isBefore(LocalDate.now())) {
                throw new BidEndedException("Product cannot be deleted. Bid end date is completed");
            }
        }
        log.debug("validateBidEndDateOfProduct passed");
    }

    private void sendProductDeleteNotification(ProductDeleteInputDTO productDeleteInputDTO) {
        String message = "Product deletion initiated for product id: "+ productDeleteInputDTO.getProductId();
        PublishResponse response = snsClient.publish(PublishRequest.builder()
                .topicArn("arn:aws:sns:eu-west-2:655138538071:product-delete-topic")
                .message(message).build());
        log.debug("SNS Publish Response: {}", response);
        snsClient.close();
    }


}