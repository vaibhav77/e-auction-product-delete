package com.eauction.seller.deleteproduct.entity;

import lombok.Data;

@Data
public class BuyerBidDetail {
    private String productId;
    private String bidAmount;
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String state;
    private String pin;
    private String phone;
    private String email;
}