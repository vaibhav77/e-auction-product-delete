package com.eauction.seller.deleteproduct.dto;

import lombok.Data;

@Data
public class ProductDeleteInputDTO {
    private String productId;
}
